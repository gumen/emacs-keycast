;; -*- lexical-binding: t; indent-tabs-mode: nil; -*-
;;; my simple modeline keycas

(require 'seq)

(defvar keycast-status "" "Last used binding with corresponding command.")
(defvar keycast-repeat 1 "How many times last binding was used in a row.")

(defun keycast-update ()
  "Update values of `keycast-status' and `keycast-repeat' base on
last pressed binding and corresponding command.  Make sure
current mode-line contains `keycast-status' and force update of
mode-line in all visible buffers."
  (when (and
         ;; Ignore undefined bindings
         this-command

         ;; Ignore regular typing
         (not (string-match ".*self-insert-command.*"
                            (format "%s" this-command)))

         ;; TODO Ignore mouse drag
         
         ;; Ignore minibuffer commands
         (not (string-match (format ".+%s" this-command)
                            (format "%s" (this-command-keys)))))

    ;; TODO does not work for kill-line (C-k)
    (setq keycast-repeat
          (if (eq last-command this-command)
              (1+ keycast-repeat) 1))

    (setq keycast-status
          (let ((key (format " %s" (key-description (this-command-keys))))
                (command (format " %s" this-command))
                (repeat (if (> keycast-repeat 1)
                            (format " x%s " keycast-repeat) " ")))

            (concat (propertize key     'face '(:inherit (mode-line-inactive) :weight bold))
                    (propertize command 'face '(:inherit (mode-line-inactive) :slant italic))
                    (propertize repeat  'face '(:inherit (mode-line-inactive)))))))

  ;; Calendar have custom mode-line-format that is not a list so
  ;; add-to-list produce error.  I don't want to have keycast in
  ;; buffers like that anyway so lets avoid similar issues.
  (if (listp mode-line-format)
      (add-to-list 'mode-line-format
                   '((:eval (default-value 'keycast-status))) t))

  ;; Update mode-line in all visible buffers.  Thanks to that we can
  ;; see pressed keys while being in minibuffer.
  (seq-each (lambda (buffer)
              (with-current-buffer buffer (force-mode-line-update)))
            (seq-map (apply-partially 'window-buffer) (window-list))))

(defun keycast-start ()
  "Log in mode-line last pressed binding with corresponding
function."
  (interactive)
  (add-hook 'pre-command-hook 'keycast-update 90)
  (add-hook 'window-state-change-hook 'keycast-update))

(defun keycast-stop ()
  "Disable keycast."
  (interactive)
  (remove-hook 'pre-command-hook 'keycast-update)
  (remove-hook 'window-state-change-hook 'keycast-update)
  (setq keycast-status "")
  (force-mode-line-update))
